import numpy as np
import matplotlib.pyplot as plt
from collections import deque

# Request structure
class Req:
    def __init__(self, num, time_in):
        self.num = num
        self.time_in = time_in
        self.time_out = 0

# Number of requests
N = 20000
# Time of service
T = 1   # Determined

# Simulate asynchronous system

x = np.arange(0.1, 1, 0.1)

def calculate_async():
    exp_d = []
    the_d = []
    print('Asynchronous:')
    for lamda in x:
        print('Lamda is', lamda)

        # Create an infinite queue
        queue = deque()

        # Generate poisson values
        Y = []
        Y = np.random.exponential(scale=1 / lamda, size=N)

        time = 0
        D = 0
        num = 0

        # Create list of requests 
        for y in Y:
            num += 1
            time += y
            queue.append(Req(num, time))

        for index, item in enumerate(queue):
            if queue[index].time_in > queue[index - 1].time_out or index == 0:
                # Razruv
                queue[index].time_out = (queue[index].time_in) + T
            else:
                queue[index].time_out = (queue[index - 1].time_out) + T

        for item in queue:
            D += item.time_out - item.time_in

        D /= N
        print('D is:', D)
        print('D should be:', (2 - lamda) / (2 * (1 - lamda)))
        exp_d.append(D)
        the_d.append((2 - lamda) / (2 * (1 - lamda)))

    plt.plot(x, exp_d, label='experimental')
    plt.plot(x, the_d, label='theoretical')

    plt.xlabel('lambda')
    plt.ylabel('d')

    plt.title("Async")

    plt.legend()

    plt.show()

def calculate_sync():
    exp_d = []
    the_d = []
    print('Synchronous:')

    for lamda in x:
        print('Lamda is', lamda)

        # Create an infinite queue
        queue = deque()
        requests = []

        # Generate poisson values
        Y = []
        Y = np.random.exponential(scale=1 / lamda, size=N)

        time = 0
        D = 0
        num = 0
        win_num = 0

        for y in Y:
            num += 1
            time += y
            requests.append(Req(num, time))

        num = 0
        while num < len(requests):
            win_num += 1
            # Send from queue
            if queue:
                request = queue.pop()
                request.time_out = win_num * T + 1

            # Check if we have new request in this window
            while num < len(requests) and requests[num].time_in < (win_num * T + 1):
                # The we have new request
                queue.append(requests[num])
                num += 1

        # Clear the queue
        while queue:
            win_num += 1
            request = queue.pop()
            request.time_out = win_num * T + 1

        for item in requests:
            D += item.time_out - item.time_in

        D /= N

        print('D is:', D)
        print('D should be:', (3 - 2 * lamda) / (2 * (1 - lamda)))
        exp_d.append(D)
        the_d.append((3 - 2 * lamda) / (2 * (1 - lamda)))

    plt.plot(x, exp_d, label='experimental')
    plt.plot(x, the_d, label='theoretical')

    plt.xlabel('lambda')
    plt.ylabel('d')

    plt.title("Sync")

    plt.legend()

    plt.show()

calculate_async()
calculate_sync()
